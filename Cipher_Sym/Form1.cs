﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cipher_Sym
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Sel_Execute_Fwd.Checked = true;
            Sel_Execute_Rev.Checked = false;
        }

        private void Bn_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Sel_Execute_Fwd_CheckedChanged(object sender, EventArgs e)
        {
            TB_Plain.ReadOnly   = false;
            TB_Cipher.ReadOnly  = true;
        }

        private void Sel_Execute_Rev_CheckedChanged(object sender, EventArgs e)
        {
            TB_Plain.ReadOnly   = true;
            TB_Cipher.ReadOnly  = false;
        }

        // Генератор случайного ключа:
        private void Bn_Keygen_Click(object sender, EventArgs e)
        {
            UInt16 size = (ushort)(Num_KeyLength.Value);
            Char maxchar = (char)(255);

            var rand = new Random();
            string key = "";
            while(key.Length < size)
            {
                key += (char)(rand.Next(0, maxchar));
            }

            TB_Key.Text = key;
        }

        // Произвести шифрование или дешифрование:
        private void Bn_Execute_Click(object sender, EventArgs e)
        {
            string key = TB_Key.Text;
            if (key.Length > 0)
            {
                string workstr = "";
                if (Sel_Execute_Fwd.Checked) { workstr = TB_Plain.Text;  }
                if (Sel_Execute_Rev.Checked) { workstr = TB_Cipher.Text; }
                if (workstr.Length > 0)
                {
                    while (workstr.Length % key.Length != 0)
                    {
                        workstr += (char)(0);
                    }
                    Progress_Execute.Maximum = workstr.Length;
                    Progress_Execute.Value = 0;

                    string result = "";
                    for (int c = 0; c < workstr.Length; c++)
                    {
                        result += (char)(workstr[c] ^ key[c % key.Length]);
                        Progress_Execute.Value = c + 1;
                    }

                    if (Sel_Execute_Fwd.Checked) { TB_Cipher.Text = result; }
                    if (Sel_Execute_Rev.Checked) { TB_Plain.Text = result;  }
                }
                else
                {
                    MessageBox.Show("Пожалуйста, введите текст для обработки.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("Пожалуйста, сгенерируйте ключ.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        // Загрузить текст из файла:
        private void BnOpen_Click(object sender, EventArgs e)
        {
            DialogResult DR = DlgOpen.ShowDialog();
            if (DR == DialogResult.OK)
            {
                var FileContent = String.Empty;
                try
                {
                    using (FileStream fs = new FileStream(DlgOpen.FileName, FileMode.Open, FileAccess.Read))
                    {
                        byte[] Bytes = new byte[fs.Length];

                        int NumBytesToRead = (int)(fs.Length);
                        int NumBytesRead = 0;

                        while (NumBytesToRead > 0)
                        {
                            int n = fs.Read(Bytes, NumBytesRead, NumBytesToRead);

                            if (n == 0) { break; }

                            NumBytesRead += n;
                            NumBytesToRead -= n;
                        }

                        var wcencod = new UnicodeEncoding();
                        FileContent = wcencod.GetString(Bytes);
                    }
                }
                catch (FileNotFoundException ioEx)
                {
                    MessageBox.Show(ioEx.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                if (FileContent.Length > 0)
                {
                    if (Sel_Execute_Fwd.Checked) { TB_Plain.Text = FileContent; }
                    if (Sel_Execute_Rev.Checked) { TB_Cipher.Text = FileContent; }
                }
                else
                {
                    MessageBox.Show("Файл не содержит текстовой информации.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                if (DR != DialogResult.Cancel)
                {
                    MessageBox.Show("Произошла ошибка при открытии файла.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        // Сохранить текст в файл:
        private void BnSave_Click(object sender, EventArgs e)
        {
            var FileContent = String.Empty;
            if (Sel_Execute_Fwd.Checked) { FileContent = TB_Cipher.Text; }
            if (Sel_Execute_Rev.Checked) { FileContent = TB_Plain.Text; }

            if (FileContent.Length > 0)
            {
                DialogResult DR = DlgSave.ShowDialog();
                if (DR == DialogResult.OK)
                {
                    var FilePath = DlgSave.FileName;
                    if (FilePath.Length > 0)
                    {
                        var wcencod = new UnicodeEncoding();
                        byte[] Result = wcencod.GetBytes(FileContent);

                        using (FileStream fs = File.Open(FilePath, FileMode.OpenOrCreate))
                        {
                            fs.Write(Result, 0, Result.Length);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Не удалось создать пригодный для записи файл.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    if (DR != DialogResult.Cancel)
                    {
                        MessageBox.Show("Произошла ошибка при открытии файла.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Отсутствует пригодный для сохранения текст.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
